package com.controller;

import com.subcription.Subcription;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import redis.clients.jedis.Jedis;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

public class MonitoringServicesController implements Job {

    Jedis jedis = new Jedis("localhost", 6379);

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        try {
            for (String subsc : Subcription.listName) {
                if (jedis.get(String.valueOf(subsc)).equals("true")) {
                    String string = String.valueOf(subsc);
                    String[] parts = string.split("-");
                    String SubsName = parts[1];
                    String SourceName = parts[2];
                    String TargetName = parts[3];

                    URL url = new URL(Subcription.URL_API + SubsName + "/" + SourceName + "/" + TargetName);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("GET");
                    conn.setRequestProperty("Accept", "application/json");
                    if (conn.getResponseCode() != 200) {
                        throw new RuntimeException("Failed : HTTP Error code : "
                                + conn.getResponseCode());
                    }
                    InputStreamReader in = new InputStreamReader(conn.getInputStream());
                    BufferedReader br = new BufferedReader(in);
                    String output;
                    output = br.readLine();
                    System.out.println(output + " " + new Date());
                    conn.disconnect();
                }
            }
//        if (jedis.get(Subcription.RedisSubTLLOG_API).equals("true")) {
//            URL url = new URL(Subcription.URL_API + Subcription.SubTLLOG_API + "/" + Subcription.SubTLLOG_API_SOURCE + "/" + Subcription.SubTLLOG_API_TARGET);//your url i.e fetch data from .
//            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//            conn.setRequestMethod("GET");
//            conn.setRequestProperty("Accept", "application/json");
//            if (conn.getResponseCode() != 200) {
//                throw new RuntimeException("Failed : HTTP Error code : "
//                        + conn.getResponseCode());
//            }
//            InputStreamReader in = new InputStreamReader(conn.getInputStream());
//            BufferedReader br = new BufferedReader(in);
//            String output;
//            output = br.readLine();
//            System.out.println(output +" "+ new Date());
//            conn.disconnect();
//
//        }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}
