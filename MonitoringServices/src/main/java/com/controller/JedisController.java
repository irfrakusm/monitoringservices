package com.controller;

import com.subcription.Subcription;
import org.springframework.web.bind.annotation.*;
import redis.clients.jedis.Jedis;

import java.util.Date;

@RestController
@RequestMapping("/Monitoring")
@CrossOrigin
public class JedisController {



    Jedis jedis = new Jedis("localhost", 6379);

    @PostMapping("/Status/{Subcription}/{Source}/{Target}/{StatusValue}")
    public void setMonitoring(@PathVariable("Subcription") String Subcription,
                              @PathVariable("Source") String Source,
                              @PathVariable("Target") String Target,
                              @PathVariable("StatusValue") Boolean StatusValue){

        jedis.set("Services-"+Subcription+"-"+Source+"-"+Target, String.valueOf(StatusValue));

    }


}
