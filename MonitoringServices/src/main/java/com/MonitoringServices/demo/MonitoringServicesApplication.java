package com.MonitoringServices.demo;

import com.controller.MonitoringServicesController;
import lombok.SneakyThrows;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RestController;

@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
@SpringBootApplication
@RestController
@ComponentScan(basePackages = {"com.controller"})
public class MonitoringServicesApplication {

	@SneakyThrows
	public static void main(String[] args) {
		SpringApplication.run(MonitoringServicesApplication.class, args);
		System.out.println("Monitoring running!!!");

		JobDetail Job = JobBuilder.newJob(MonitoringServicesController.class).build();

		Trigger trigger = TriggerBuilder.newTrigger().withIdentity("CroneTrigger")
							.withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(2)
							.repeatForever()).build();

		try {
			Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
			scheduler.start();
			scheduler.scheduleJob(Job,trigger);
		} catch (SchedulerException e) {
			e.printStackTrace();
		}


	}

}
